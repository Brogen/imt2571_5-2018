<?php
/**
  * This file is a part of the code used in IMT2571 Assignment 5.
  *
  * @author Rune Hjelsvold
  * @version 2018
  */

require_once('Club.php');
require_once('Skier.php');
require_once('YearlyDistance.php');
require_once('Affiliation.php');

/**
  * The class for accessing skier logs stored in the XML file
  */  
class XmlSkierLogs
{
    /**
      * @var DOMDocument The XML document holding the club and skier information.
      */  
    protected $doc;
    protected $xpath;
    /**
      * @param string $url Name of the skier logs XML file.
      */  
    public function __construct($url)
    {
        $this->doc = new DOMDocument();
		//$this->doc->preserveWhiteSpace = false;
        $this->doc->load($url);
		$this->xpath = new DOMXPath($this->doc);
    }
    
    /**
      * The function returns an array of Club objects - one for each
      * club in the XML file passed to the constructor.
      * @return Club[] The array of club objects
      */
    public function getClubs()
    {
        $clubs = array();
		$clubList = $this->xpath->query('/SkierLogs/Clubs/Club');
		
		for($i = 0; $i < $clubList->length; $i++) {
			$club = $clubList->item($i);
			$children = $club->firstChild;
			
			$id = $club->getAttribute('id');
			
			while($children->nodeName != 'Name') $children = $children->nextSibling;
			$name = $children->textContent;
			
			while($children->nodeName != 'City') $children = $children->nextSibling;
			$city = $children->textContent;
			
			while($children->nodeName != 'County') $children = $children->nextSibling;
			$county = $children->textContent;
			
			$clubs[$i] = new Club($id, $name, $city, $county);
		}
        
        return $clubs;
    }

    /**
      * The function returns an array of Skier objects - one for each
      * Skier in the XML file passed to the constructor. The skier objects
      * contains affiliation histories and logged yearly distances.
      * @return Skier[] The array of skier objects
      */
    public function getSkiers()
    {
        $skiers = array();
		$skierList = $this->xpath->query('/SkierLogs/Skiers/Skier');
		
		for($i = 0; $i < $skierList->length; $i++) {
			$skier = $skierList->item($i);
			$skierChild = $skier->firstChild;
			$username = $skier->getAttribute('userName');
			
			while($skierChild->nodeName != 'FirstName') $skierChild = $skierChild->nextSibling;
			$firstname = $skierChild->textContent;
			
			while($skierChild->nodeName != 'LastName') $skierChild = $skierChild->nextSibling;
			$lastname = $skierChild->textContent;
			
			while($skierChild->nodeName != 'YearOfBirth') $skierChild = $skierChild->nextSibling;
			$yearofbirth = $skierChild->textContent;
		
			$skiers[$i] = new Skier($username, $firstname, $lastname, $yearofbirth);
			
			$affiliations = array();
			$affIndex = 0;
			$affiliationList = $this->xpath->query('/SkierLogs/Season/Skiers[@clubId]/Skier');
			
			for ($j = 0; $j < $affiliationList->length; $j++) {
				$affChild = $affiliationList->item($j);
				$affName = $affChild->getAttribute('userName');
				
				if($username == $affName) {
					$clubId = $affChild->parentNode->getAttribute('clubId');
					$affSeason = $affChild->parentNode->parentNode->getAttribute('fallYear');
					
					$affiliations[$affIndex] = new Affiliation($clubId, $affSeason);
					$affIndex++;
				}
			}
			if($affIndex > 0) {
				$skiers[$i]->addAffiliations($affiliations);
			}
			
			$yearlydistances = array();
			$ydIndex = 0;
			$yearlydistanceList = $this->xpath->query('/SkierLogs/Season/Skiers/Skier');
			
			for ($k = 0; $k < $yearlydistanceList->length; $k++) {
				$ydChild = $yearlydistanceList->item($k);
				$ydName = $ydChild->getAttribute('userName');
				
				if($username == $ydName) {
					$ydSeason = $ydChild->parentNode->parentNode->getAttribute('fallYear');
					$distanceKm = $this->xpath->evaluate('sum(./Log/Entry/Distance)', $ydChild);
					
					$yearlydistances[$ydIndex] = new YearlyDistance($ydSeason, $distanceKm);
					$ydIndex++;
				}
			}
			if($ydIndex > 0) {
				$skiers[$i]->addYearlyDistances($yearlydistances);
			}
		}
		
		
        // TODO: Implement the function retrieving skier information,
        //       including affiliation history and logged yearly distances.
        return $skiers;
    }
}

?>